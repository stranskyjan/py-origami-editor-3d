extensions = [
    'sphinx.ext.autodoc',
]
source_suffix = '.rst'
master_doc = 'index'
project = 'py-origami-editor-3d'
author = 'Jan Stránský'
copyright = '2022, {}'.format(author)

version = '0.1'
release = '0.1.0'
pygments_style = 'sphinx'
html_theme = 'classic'
autodoc_member_order = 'bysource'

autodoc_type_aliases = {
    'Vector2': 'Vector2',
    'Vector3': 'Vector3',
    'Vector': 'Vector',
    'Number': 'Number',
}
