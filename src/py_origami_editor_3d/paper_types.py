A4 = "a4"
"""a4"""

SQUARE = "square"
"""square"""

HEXAGON = "hexagon"
"""hexagon"""

USG = "usg"
"""usg"""

HUF = "huf"
"""huf"""

types = (A4, SQUARE, HEXAGON, USG, HUF)
"""types usable in Paper command"""
