######################################################################
# Makefile for py-origami-editor-3d project
# Run 'make help' for more info
######################################################################

######################################################################
# settable variables
######################################################################
PYTHON=python3
PREFIX=
USER=
SPHINX_APIDOC_COMMAND=sphinx-apidoc
SPHINX_BUILD_COMMAND=sphinx-build
SPHINX_BUILD_DIR=build/doc
APIDOC_DIR=build/apidoc

######################################################################
# auxiliary functions and variables
######################################################################

_PYTHON_HELP="    PYTHON ... sets python executable (e.g. PYTHON='python'). Defaults is 'python3'"

######################################################################
# targets
######################################################################
.PHONY: doc examples

help:
	@echo
	@echo "py-origami-editor-3d makefile"
	@echo
	@echo "targets:"
	@echo "  install"
	@echo "    runs 'python3 setup.py install' to installs the application"
	@echo "    options:"
	@echo "    USER=1 ... adds '--user' switch to the install command"
	@echo "    PREFIX=/your/prefix ... adds '--prefix=/your/prefix' to the install commands"
	@echo $(_PYTHON_HELP)
	@echo "  examples"
	@echo "    run examples"
	@echo $(_PYTHON_HELP)
	@echo "  doc"
	@echo "    create Sphinx HTML documentation"
	@echo "    options:"
	@echo "      SPHINX_BUILD_COMMAND ... sphinx build command (default is 'sphinx-build')"
	@echo "      SPHINX_BUILD_DIR     ... target directory (default is 'build/doc')"
	@echo "  examples"
	@echo "    build all examples"
	@echo "  clean"
	@echo "    cleans intermediate and auxiliary files"
	@echo "  dist"
	@echo "    creates distribution for PyPI by 'python3 setup.py sdist bdist_wheel' command TODO"


INSTALLCMD=$(PYTHON) -m pip install .
install:
ifneq ($(USER),)
	$(INSTALLCMD) --user
else ifneq ($(PREFIX),)
	$(INSTALLCMD) --prefix $(PREFIX)
else
	$(INSTALLCMD)
endif


doc:
	$(SPHINX_APIDOC_COMMAND) -H py-origami-editor-3d -A "Jan Stránský" -V 0.1 -R 0.1.0 -o $(APIDOC_DIR) src/py_origami_editor_3d
	cp doc/conf.py $(APIDOC_DIR)
	cp doc/index.rst $(APIDOC_DIR)
	$(SPHINX_BUILD_COMMAND) -b html -a -E $(APIDOC_DIR) $(SPHINX_BUILD_DIR)/html


examples:
	@export PYTHONPATH="$${PYTHONPATH}:src"; \
	for f in examples/*.py; do \
		echo $$f; \
		$(PYTHON) $$f; \
	done


clean:
	rm -rf build
	rm -rf dist
	rm -rf src/*.egg-info
	rm -rf src/*/__pycache__
	rm -f examples/*.txt


dist:
	make clean
	python3 -m build --no-isolation
